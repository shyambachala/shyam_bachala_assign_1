package myArrayList.test;

import myArrayList.MyArrayList;
import myArrayList.util.Results;

public class MyArrayListTest 
{
//	  Test method for testing the arraylist
	  public void testMe(MyArrayList myArrayList, Results results)
	  {
		  results.storeNewResult("MYARRAYLIST: ELEMENTS AFTER READING THE FILE\n");
	      results.storeNewResult(myArrayList.toString());
	      results.storeNewResult("\n-----------------------------------------------------------\n");
	      
	      results.storeNewResult("SUM OF ALL THE VALUES IN THE ARRAY LIST IS :" + myArrayList.sum() + "\n");
	      results.storeNewResult("\n-----------------------------------------------------------\n");
	      
	      testMethod1(myArrayList, results);
		  testMethod2(myArrayList, results);
		  testMethod3(myArrayList, results);
		  testMethod4(myArrayList, results);
		  testMethod5(myArrayList, results);
		  testMethod6(myArrayList, results);
		  testMethod7(myArrayList, results);
		  testMethod8(myArrayList, results);
		  testMethod9(myArrayList, results);
		  testMethod10(myArrayList, results);
     
	  }
	  
	  public void testMethod1(MyArrayList myArrayList, Results results)
	  {
		  int numSize = myArrayList.size();
		  int samsum = myArrayList.sum();
		  results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 12\n");
	      myArrayList.removeValue(12);
	      results.storeNewResult(myArrayList.toString());
	    
	            
	      if((numSize == (myArrayList.size() + 1)) && (myArrayList.sum() == (samsum - 12)))
	      {
	    	  results.storeNewResult("\nSIZE_BEFORE:" + numSize + "-----" + "SIZE_NOW :" + (myArrayList.size()) + "\n" );
	    	  results.storeNewResult("\nTESTMETHOD 1 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	      else
	      {
	    	  results.storeNewResult("\nSIZE_BEFORE:" + numSize + "-----" + "SIZE_NOW :" + (myArrayList.size()) + "\n" );
	    	  results.storeNewResult("\nTESTMETHOD 1 --> FAILED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	  }
	  
	  public void testMethod2(MyArrayList myArrayList, Results results)
	  {
		  int numSize = myArrayList.size();
		  int samsum = myArrayList.sum();
		
		  results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 124\n");
	      myArrayList.removeValue(124);
	      
	      results.storeNewResult(myArrayList.toString());
	    
	      results.storeNewResult("SIZE OF THE ARRAY -->" + myArrayList.size() + "\n");
	      
	      if((numSize == (myArrayList.size())) && (myArrayList.sum() == (samsum)))
	      {
	    	  results.storeNewResult("\nINDEXOF(124):" + myArrayList.indexOf(124) + "\n" +"\nSIZE_BEFORE:" + numSize + "-----" + "SIZE_NOW :" + (myArrayList.size()) + "\n");
	    	  results.storeNewResult("\nTESTMETHOD 2 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	      else
	      {
	    	  results.storeNewResult("\nINDEXOF(124):" + myArrayList.indexOf(124) + "\n" +"\nSIZE_BEFORE:" + numSize + "-----" + "SIZE_NOW :" + (myArrayList.size()) + "\n");
		      results.storeNewResult("\nTESTMETHOD 2 --> FAILED\n");
		      results.storeNewResult("\n-----------------------------------------------------------\n");
	      }  
	  }

	  public void testMethod3(MyArrayList myArrayList, Results results)
	  {
		  int samsum = myArrayList.sum();
		
		  results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 777\n");
	      myArrayList.insertSorted(777);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 333\n");
	      myArrayList.insertSorted(333);
	      
	      results.storeNewResult(myArrayList.toString());
	    
	      if((myArrayList.indexOf(333) == 3 ))
	      {
	    	  results.storeNewResult("\nINDEX(333) :" + (myArrayList.indexOf(333)) + "\n");
	    	  results.storeNewResult("\nTESTMETHOD 3 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	      else
	      {
	    	  results.storeNewResult("\nINDEX(333) :" + (myArrayList.indexOf(333)) + "\n");
		   	  results.storeNewResult("\nTESTMETHOD 3 --> FAILED\n");
		   	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }  
	  }
	  
	  public void testMethod4(MyArrayList myArrayList, Results results)
	  {
		  int index1 = -100;
		  int index2 = -100;
		  
		  results.storeNewResult(myArrayList.toString() + "\n");
		  
		  results.storeNewResult("MYARRAYLIST: SEARCH AN ELEMENT: 1234\n");
	      index1 = myArrayList.indexOf(1234);
	    
	      results.storeNewResult("MYARRAYLIST: SEARCH AN ELEMENT: 7564\n");
	      index2 = myArrayList.indexOf(7564);
	    
	      results.storeNewResult("\n");
	      
	      if((index1 == 5 ) && (index2 == -1))
	      {
	    	  results.storeNewResult("\nINDEX(7564) :" + (myArrayList.indexOf(7564)) + "\n");
	    	  results.storeNewResult("TESTMETHOD 4 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	      else
	      {
	    	  results.storeNewResult("\nINDEX(7564) :" + (myArrayList.indexOf(7564)) + "\n");
		   	  results.storeNewResult("TESTMETHOD 4 --> FAILED\n");
		   	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }    
	  }
	  
	  public void testMethod5(MyArrayList myArrayList, Results results)
	  {
		  int index1 = -100, index2 = -100;
		  
		  results.storeNewResult("MYARRAYLIST: SEARCH AN ELEMENT: 3548\n");
	      index1 = myArrayList.indexOf(3548);
	    
	      results.storeNewResult("\nINDEX(3548) :" + (index1) + "\n");
	      
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 3548\n");
	      myArrayList.insertSorted(3548);
	      
	      results.storeNewResult(myArrayList.toString());
	    
	      results.storeNewResult("MYARRAYLIST: SEARCH AN ELEMENT: 3548\n");
	      index2 = myArrayList.indexOf(3548);
	    
	      results.storeNewResult("\n");
	     	      
	      if((index1 == -1 ) && (index2 != -1))
	      {
	    	  results.storeNewResult("\nINDEX(3548) :" + (index2) + "\n");
	    	  results.storeNewResult("TESTMETHOD 5 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	      else
	      {
	    	  results.storeNewResult("\nINDEX(3548) :" + (index2) + "\n");
	    	  results.storeNewResult("TESTMETHOD 5 --> FAILED\n");
		   	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }    
	  }
	  
	  public void testMethod6(MyArrayList myArrayList, Results results)
	  {
		  int index1 = -100, index2 = -100;
		  
		  results.storeNewResult("MYARRAYLIST: SEARCH AN ELEMENT: 1\n");
	      index1 = myArrayList.indexOf(1);
	      results.storeNewResult("\nINDEX(1) :" + (index1) + "\n");
	    
	      results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 1\n");
	      myArrayList.removeValue(1);
	     
	      results.storeNewResult(myArrayList.toString());
	    
	      results.storeNewResult("MYARRAYLIST: SEARCH AN ELEMENT: 1\n");
	      index2 = myArrayList.indexOf(1);
	    
	      results.storeNewResult("\n");
	     
	      
	      if((index1 != -1 ) && (index2 == -1))
	      {
	    	  results.storeNewResult("\nINDEX(1) DELETED ELEMENT :" + (myArrayList.indexOf(1)) + "\n");
	    	  results.storeNewResult("TESTMETHOD 6 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	      else
	      {
	    	  results.storeNewResult("\nINDEX(1) DELETED ELEMENT :" + (myArrayList.indexOf(1)) + "\n");
		   	  results.storeNewResult("TESTMETHOD 6 --> FAILED\n");
		   	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }  
	  }
	  
	  public void testMethod7(MyArrayList myArrayList, Results results)
	  {
		  int samSum2 = 0;
		  int samSum = 0;
	      results.storeNewResult(myArrayList.toString());
		  
		  
		  results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 333\n");
	      myArrayList.insertSorted(333);
	        
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 333\n");
	      myArrayList.insertSorted(333);

	      samSum = myArrayList.sum();
	      
	      results.storeNewResult(myArrayList.toString());
	        
	      results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 333\n");
	      myArrayList.removeValue(333);
	      results.storeNewResult(myArrayList.toString());
	    
	      results.storeNewResult("\n");
	      samSum2 = myArrayList.sum();
	      
	      if(samSum2 == (samSum - 999) )
	      {
	    	  results.storeNewResult("\nSUM AFTER INSERTION:" + samSum + "-----" + "SUM AFTER DELETION OF 333(3 OCCURENCES) :" + (samSum2) + "\n");
	    	  results.storeNewResult("TESTMETHOD 7 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	      else
	      {
	    	  results.storeNewResult("\nSUM AFTER INSERTION:" + samSum + "-----" + "SUM AFTER DELETION OF 333(3 OCCURENCES) :" + (samSum2) + "\n");
	    	  results.storeNewResult("TESTMETHOD 7 --> FAILED\n");
		   	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }    
		  
	  }
	  
	  public void testMethod8(MyArrayList myArrayList, Results results)
	  {
		  int numSize = myArrayList.size();
		  			
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 845\n");
	      myArrayList.insertSorted(845);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 3489\n");
	      myArrayList.insertSorted(3489);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 57\n");
	      myArrayList.insertSorted(57);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 2897\n");
	      myArrayList.insertSorted(2897);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 8\n");
	      myArrayList.insertSorted(8);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 11111\n");
	      myArrayList.insertSorted(11111);
	    
	      results.storeNewResult(myArrayList.toString());
	    
	      results.storeNewResult("\n");
	     
	      
	      if(numSize == (myArrayList.size() - 5))
	      {
	    	  results.storeNewResult("INPUT(11111) NOT ACCEPTED\n");
	    	  results.storeNewResult("\nSIZE_BEFORE:" + numSize + "-----" + "SIZE_NOW :" + (myArrayList.size()) + "\n");
	    	  results.storeNewResult("TESTMETHOD 8 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");  
	      }
	      else
	      {
	    	  results.storeNewResult("INPUT(11111) NOT ACCEPTED\n");
	    	  results.storeNewResult("\nSIZE_BEFORE:" + numSize + "-----" + "SIZE_NOW :" + (myArrayList.size()) + "\n");
	    	  results.storeNewResult("TESTMETHOD 8 --> FAILED\n");
		   	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }    
		  
	  }
	  
	  public void testMethod9(MyArrayList myArrayList, Results results)
	  {
		  int numSize1 = 0;
		  int numSize2 = 0;
		  results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 6745\n");
	      myArrayList.insertSorted(6745);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 6745\n");
	      myArrayList.insertSorted(6745);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 6745\n");
	      myArrayList.insertSorted(6745);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 6745\n");
	      myArrayList.insertSorted(6745);
	    
	      results.storeNewResult("MYARRAYLIST: INSERT AN ELEMENT: 6745\n");
	      myArrayList.insertSorted(6745);
	      results.storeNewResult(myArrayList.toString());
		    
	      numSize1 = myArrayList.size();
	      
	      results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 6745\n");
	      myArrayList.removeValue(6745);
	    
	      numSize2 = myArrayList.size();
	      results.storeNewResult("\n");
	      results.storeNewResult(myArrayList.toString());
	      
	      if(numSize1 == (numSize2 + 5))
	      {
	    	  results.storeNewResult("\nSIZE_BEFORE:" + numSize1 + "-----" + "SIZE_NOW :" + (numSize2) + "\n");
	    	  results.storeNewResult("TESTMETHOD 9 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	      else
	      {
	    	  results.storeNewResult("\nSIZE_BEFORE:" + numSize1 + "-----" + "SIZE_NOW :" + (numSize2) + "\n");
	    	  results.storeNewResult("\nTESTMETHOD 9 --> FAILED\n\n");
		   	  results.storeNewResult("\n-----------------------------------------------------------\n");
		    	   
	      }
	  }
	  
	  public void testMethod10(MyArrayList myArrayList, Results results)
	  {
		  int numSize = myArrayList.size();
		  int samSum = myArrayList.sum();
		  int text = 0;
		  	      
		  results.storeNewResult(myArrayList.toString() + "\n");
		  
		  results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 0\n");
	      myArrayList.removeValue(0);
	      text += 57;
	      results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 57\n");
	      myArrayList.removeValue(57);
	      
	      results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 777\n");
	      myArrayList.removeValue(777);
	      
	      results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 845\n");
	      myArrayList.removeValue(845);
	      
	      results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 1234\n");
	      myArrayList.removeValue(1234);
	      
	      results.storeNewResult("MYARRAYLIST: DELETE AN ELEMENT: 3489\n");
	      myArrayList.removeValue(3489);
	      
	      results.storeNewResult(myArrayList.toString());
		  
	      text = 57 + 777 + 845 + 1234 + 3489;
	      if((numSize == (myArrayList.size() + 6)) && (myArrayList.sum() == (samSum - text)))
	      {
	    	  results.storeNewResult("\nSUM AND SIZE ARE EQUAL\n");
	    	  results.storeNewResult("\nSIZE_BEFORE:" + numSize + "-----" + "SIZE_NOW :" + (myArrayList.size()) + "\n");
	    	  results.storeNewResult("\nTESTMETHOD 10 --> PASSED\n");
	    	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	      else
	      {
	    	  results.storeNewResult("\nSUM AND SIZE ARE EQUAL\n");
	    	  results.storeNewResult("\nSIZE_BEFORE:" + numSize + "-----" + "SIZE_NOW :" + (myArrayList.size()) + "\n");
	    	  results.storeNewResult("\nTESTMETHOD 10 --> FAILED\n");
		   	  results.storeNewResult("\n-----------------------------------------------------------\n");
	      }
	  }
}
