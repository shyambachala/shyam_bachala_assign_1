package myArrayList;
import java.util.Arrays;

public class MyArrayList
{
	  private int[] mySeq;
	  private int[] helperSeq;
	  private int indexSeq;

	  //constructor 
	  public MyArrayList()
	  {
		  mySeq = new int[50];
		  indexSeq = 0;
	  }
	  
//	  Method to print the formatted strings
	  public String toString()
	  {
		  String concat_int = "";
		  for(int i=0; i <indexSeq; i++)
		  {
			  concat_int = concat_int + mySeq[i] + "\n";
		  }
		  return concat_int;
	  }
	  
//	  Returns the value of the position of the element in the data structure
	  public int indexOf(int value)
	  {
		  int found = -1;
		  for(int i = 0; i < indexSeq; i++)
		  {
			  if(mySeq[i] == value)
			  {
				  found = i;
				  return found;
			  }
		  }
		  return found;
	  }
	  
	  //## function to return the size of the filled array
	  public int size()
	  {
		  return indexSeq;
	  }
	  
	  //## returns the sum of the values of the array elements
	  public int sum()
	  {
		  int totalSum = 0;
		  for(int i = 0; i < indexSeq; i++)
		  {
			  totalSum += mySeq[i];
		  }
		  return totalSum;
	  }

	  //## function to insert an element
	  public void insertSorted(int newValue)
	  {
		  if((newValue > 10000) || (newValue < 0))
		  {
			  return;
		  }
		  if((indexSeq + 1) < mySeq.length)
		  {
			  if((newValue > 10000) && (newValue < 0))
			  {
				  return;
			  }
			  mySeq[indexSeq++] = newValue;
			  Arrays.sort(mySeq, 0, indexSeq);
	      
		  }
	    
		  else
		  {
			  resizeBeforeInsert();
			  mySeq[indexSeq++] = newValue;
			  Arrays.sort(mySeq, 0, indexSeq);
		  }
	  }

	  //## function to resize the array when the limit is reached
	  private void resizeBeforeInsert()
	  {
		  int newSize = 0;
		  newSize = (int)(mySeq.length * 1.5);
		  helperSeq = new int[newSize];
		  for(int i=0; i < indexSeq; i++)
		  {
			  helperSeq[i] = mySeq[i];  
		  }
		  mySeq = helperSeq;
	  }
	  
	  //## function to remove a given value and its duplicates from the array
	  public void removeValue(int value)
	  {
		  if(indexSeq == 0)
		  {
			  return;
		  }
	      int occurences = 0;
	      int index_del = 0;
	      while((index_del = indexOf(value)) != -1)
	      {
	    	  mySeq[index_del] = -7;
	    	  occurences += 1;
	      }
	      
	      if(occurences == 0)
	      {
//	      System.out.println("ERROR: CANT DELETE: ELEMENT NOT FOUND IN ARRAY");
	    	  return;
	      }
	    
	      resizeAfterRemove(occurences);
	  }


	  //## private helper function to remove the element
	  private void resizeAfterRemove(int times_del)
	  {
		  int indexfromLast = indexSeq - 1;
		  int index_del = 0;
		  while((index_del = indexOf(-7)) != -1)
		  {
			  int temp = mySeq[indexfromLast];
			  mySeq[indexfromLast--] = mySeq[index_del];
			  mySeq[index_del] = temp;
			  indexSeq--;
		  }
		  Arrays.sort(mySeq, 0, indexSeq);
	  }
	  
}