package myArrayList.driver;

import java.io.File;
import java.io.IOException;

import myArrayList.MyArrayList;
import myArrayList.test.MyArrayListTest;
import myArrayList.util.FileProcessor;
import myArrayList.util.Results;

public class Driver 
{

	public static void main(String[] args) throws IOException
	{
//		Checking the number of command line parameters
		if(args.length != 2 || args[1].equals("${arg1}"))
		{
			System.out.println("INVALID NUMBER OF INPUT PARAMETERS" + args.length + "\nGIVE INPUT AS BELOW\n"
					+ "ant -buildfile src/build.xml run -Darg0=/home/input_file/input_file.txt -Darg1=/home/output_file/output_file.txt\r\n");
			System.exit(4);
		}
		else
		{
			File infile = new File(args[0]);
			File outfile = new File(args[1]);
			String inputLine;
			
//			FileProcessor and MyArrayList instances created
			FileProcessor freader = new FileProcessor(infile, outfile);
	        MyArrayList myArray = new MyArrayList();
	        Results sampleResults = new Results();
	      
//	        Reading the input file and assigning the values to the array
	        while((inputLine = freader.readLine()) != null)
	        {
	        	try
	        	{
	        		myArray.insertSorted(Integer.parseInt(inputLine));
	        	}
	        	catch(Exception e)
	        	{
	        		System.out.println("\nException : NumberFormatException\n");
	        		e.printStackTrace();
	        	}
	        	finally
	        	{}
	        }  
	      
//	        Condition checking for empty input file
	        if(myArray.size() > 0)
	        {
	        	freader.outFile();
	        }
	        else
	        {
	        	System.out.println("Error: Input file has no data");
	        	System.exit(1);
	        }
	        
//	        Invoking the test methods using MyArrayListTest
	        MyArrayListTest sampleTest = new MyArrayListTest();
	        sampleTest.testMe(myArray, sampleResults);
	        freader.remove_dev();
		}
	}
	
}
