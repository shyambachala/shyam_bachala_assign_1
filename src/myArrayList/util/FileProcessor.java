package myArrayList.util;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileProcessor 
{
	private static FileProcessor instance = null;
	FileReader file_reader = null;
	static FileWriter file_writer = null;
	static BufferedWriter buff_writer = null;
    BufferedReader buff_reader = null;
    static File fname = null;
    static File oname = null;
    
    public FileProcessor(File infilename, File outfilename)
    {
    	
        fname = infilename;
        oname = outfilename;
        
               
        try
        {
        	buff_reader = new BufferedReader(file_reader = new FileReader(infilename));
        }
        catch (FileNotFoundException ex) 
	    {
        	System.out.println("\nException : File not Found");
			ex.printStackTrace();
			System.exit(4);
        }
        finally {}
          
    }
    
//    FileWriter instances intitalized
    public void outFile()
    {
    	try 
        {
        	if(oname == null)
        		oname.createNewFile();
		} 
        catch (IOException e) 
        {
        	System.out.println("\nIO Exception : Couldn't Create Write file\n");
			e.printStackTrace();
		}
        finally
        {}
        
        try
        {
        	buff_writer = new BufferedWriter(file_writer = new FileWriter(oname));
        }
        catch (IOException ex) 
	    {
        	System.out.println("\nIO Exception : Couldn't Write file\n");
			ex.printStackTrace();
        }
        finally {}
    }
    
    
    public static FileProcessor getInstance() {
        if(instance == null) {
           instance = new FileProcessor(FileProcessor.fname, FileProcessor.oname);
        }
        return instance;
     }
    
//    Writing to the output file
    public void writeToLine(String s) 
    {
    	try 
    	{
			buff_writer.write(s);
		} 
    	catch (IOException e)
    	{
    		System.out.println("\nIO Exception \n");
			e.printStackTrace();
		}
    	finally
    	{
			if (buff_writer != null)
				try 
				{
					if (buff_writer != null)
						buff_writer.close();
					if (buff_writer != null)
						file_writer.close();
					buff_writer = new BufferedWriter(file_writer = new FileWriter(oname,true));
					
				} 
				catch (IOException e) 
				{
					System.out.println("\nIO Exception\n");
					e.printStackTrace();
				}
    	}
    	
    }
   

//  Closing the filestreams
	public  void remove_dev()
    {
      try 
      {
    	  if (file_reader != null)
    		  file_reader.close();
    	  if (buff_reader != null)
    		  buff_reader.close();
    	  if (buff_writer != null)
    		  buff_writer.close();
    	  if (file_writer != null)
    		  file_writer.close();
      } 
      catch (IOException ex) 
      {
    	  System.err.format("\nIO Exception : Trying to close files\n");
    	  ex.printStackTrace();
      }
      finally
      {}

    }
	
//	Read lines from the input file and return one line at a time
    public String readLine()
    {
        String line = "";
        try
        {
            line = buff_reader.readLine();
        }
        catch (Exception e)
        {
        	System.err.format("\nException occurred trying to read '%s'.\n", fname);
            e.printStackTrace();
        }
        finally
        { }
        return line;
    }
}
