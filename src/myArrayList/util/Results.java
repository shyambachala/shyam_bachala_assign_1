package myArrayList.util;

public class Results implements FileDisplayInterface, StdoutDisplayInterface 
{
	private String[] record;
	int index = 0;
	FileProcessor writefile = FileProcessor.getInstance();
	
//	constructor
	public Results()
	{
		record = new String[1000];
	}
	  
//	Storing the result to the result data structure
	public void storeNewResult(String s)
	{
		writeToFile(s);
	    if(index < 1000 && s != "")
	    {
	      record[index++] = s;
	    }
	}
	
//	Method to write to output file
	public void writeToFile(String s)
	{
		writefile.writeToLine(s);
	}
	  
//	Method to write to Stdout
	public void writeToStdout()
	{
		for(int i = 0; i < index; i++)
		{
			System.out.println(record[i]);
		}
	}
	
//	toString method for debugging
	  public String toString()
	  {
		  String concat_int = "";
		  for(int i=0; i <index; i++)
		  {
			  concat_int = concat_int + record[i] + "\n";
		  }
		  return concat_int;
	  }
	  
	  
}