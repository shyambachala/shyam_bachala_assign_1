IMPLEMENTATION OF ARRAY-LIST LIBRARY | JAVA

## To clean:
ant -buildfile src/build.xml clean

-----------------------------------------------------------------------
## To compile:

ant -buildfile src/build.xml all

Description: Compiles your code and generates .class files inside the BUILD
folder.

-----------------------------------------------------------------------

## To run by code:

ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=output.txt

Description:

Testing the input.txt file has been done by placing it in the [shyam_bachala_assign_1/myArraylist]
as mentioned in the design requirements.

-------------------------------------------------------------------------
## To create tarball for submission
ant -buildfile src/build.xml tarzip


------------------------------------------------------------------------

## Choice of Data Structures

1.When an array list is created it has a dynamic size allocation to that of input file.
  When the number of elements inserted exceeds the size of the array list it is dynamically resized to 1.5 times its old size.
  This leads to less space wastage as compared to of vector which increases at a pace of 2 times.

2. Using the index of the elements, elements can be accessed easily any number of times faster and efficiently
   anywhere in the scope of the array list in the program.
   According to the big O notation, the read by position is O(1)
3.It is easier to design the storage and access of the data using array list.

----------------------------------------------------------------------------------------------------------------------
